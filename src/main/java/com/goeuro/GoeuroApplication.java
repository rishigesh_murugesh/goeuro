
package com.goeuro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.goeuro.service.Util;

@SpringBootApplication
public class GoeuroApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoeuroApplication.class, args);
		Util.file_name = args[0];
	}
}
