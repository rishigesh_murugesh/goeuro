package com.goeuro.model;

import java.util.LinkedList;

public class BusRoute {
	private int route_id;
	private LinkedList<Integer> station_ids;

	public BusRoute(int id, LinkedList<Integer> routes) {
		this.setRoute_id(id);
		this.setStation_ids(routes);
	}

	public int getRoute_id() {
		return route_id;
	}

	public void setRoute_id(int route_id) {
		this.route_id = route_id;
	}

	public LinkedList<Integer> getStation_ids() {
		return station_ids;
	}

	public void setStation_ids(LinkedList<Integer> station_ids) {
		this.station_ids = station_ids;
	}

}
