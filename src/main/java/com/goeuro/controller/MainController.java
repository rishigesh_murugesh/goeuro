package com.goeuro.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.goeuro.model.BusRoute;
import com.goeuro.model.Response;
import com.goeuro.service.Util;

@RestController
@RequestMapping(path = "/rest/provider/goeurobus/direct/{dep_sid}/{arr_sid}")
public class MainController {

	@RequestMapping(method = RequestMethod.GET)
	public Response test(@PathVariable int dep_sid, @PathVariable int arr_sid) {
		List<BusRoute> routes = Util.getBusRoutes();
		for (BusRoute bus_route : routes) {
			List<Integer> station_ids = bus_route.getStation_ids();
			if (station_ids.contains(dep_sid))
				if (station_ids.contains(arr_sid))
					return new Response(dep_sid, arr_sid, true);
		}
		return new Response(dep_sid, arr_sid, false);
	}
}
