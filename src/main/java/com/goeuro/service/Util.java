package com.goeuro.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.goeuro.GoeuroApplication;
import com.goeuro.model.BusRoute;

public class Util {

	public static String file_name;

	public static List<BusRoute> getBusRoutes() {
		final Logger logger = LoggerFactory.getLogger(GoeuroApplication.class);
		final List<BusRoute> bus_routes;
		if (StringUtils.isEmpty(file_name)) {
			// Use the default sample data file if no file is given as command
			// line parameter
			file_name = "src/main/resources/data/BusRouteData.csv";
		}
		final String seperator = " ";
		try (final BufferedReader reader = new BufferedReader(new FileReader(file_name))) {
			final Integer number_of_routes = reader.lines().findFirst().map(s -> Integer.valueOf(s)).get();
			bus_routes = reader.lines().map(line -> {
				final String[] data = line.split(seperator);
				Integer bus_route_id = Integer.parseInt(data[0]);
				LinkedList<Integer> bus_route = new LinkedList<Integer>();
				for (int i = 1; i < data.length; i++) {
					bus_route.add(Integer.parseInt(data[i]));
				}
				return new BusRoute(bus_route_id, bus_route);
			}).collect(Collectors.toList());
			if (!number_of_routes.equals(bus_routes.size())) {
				logger.error("Bus routes not retrieved properly from the data file.");
			}
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
		return bus_routes;
	}
}
